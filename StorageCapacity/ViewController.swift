//
//  ViewController.swift
//  StorageCapacity
//
//  Created by Syncrhonous on 27/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
let diskInfo = DiskInformation();
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var storageStatusLabel: UILabel!
    var avail: Double = 0
    
    
    @IBAction func totalStorageBtn(_ sender: UIButton) {
        print("Total disk space is", diskInfo.getTotalDiskSpace(),"MB");
    }
    
    @IBAction func usedStorageBtn(_ sender: UIButton) {
        print("Total used space is", diskInfo.getTotalUsedSpace(),"MB");
    }
    
    @IBAction func freeStorageBtn(_ sender: UIButton) {
        
        //print("Total free space is", diskInfo.getTotalFreeSpace());
       getDiskInfo()
    }
    
    
    func getDiskInfo(){
        let s = diskInfo.getTotalFreeSpace()
        print(s)
        if (s != "memoryError"){
            avail = Double(s)!
        }
        
        
        if avail > 10.0 {
            print("true")
        }else{
            print("False")
        }
    }
    
    
    
}

